import pygame
import math
import random
from pygame import mixer
pygame.init()

win = pygame.display.set_mode((800, 600))
pygame.display.set_caption("SPACE INVADERS")

icon = pygame.image.load('rocket.png')
pygame.display.set_icon(icon)

bulletSound = mixer.Sound('laser.wav')
hitSound = mixer.Sound('explosion.wav')
music = mixer.music.load('background.wav')
mixer.music.play(-1)

score = 0

class player(object):
    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.vel = 13

    def draw(self, win):
        playerImg = pygame.image.load('player.png')
        win.blit(playerImg, (self.x ,self.y))

class enemy(object):
    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.vel = 2
        self.hitbox = (self.x, self.y, 64, 55)
        self.visible = True
        self.num = 6

    def draw(self, win):
        self.move()
        if self.visible:
            for i in range(self.num):
                for j in range(self.num):
                    if self.y > 440:
                        game_over()
                        self.y= 900
                        break

                enemyImg = pygame.image.load('enemy.png')
                win.blit(enemyImg, (self.x, self.y))
                self.hitbox = (self.x, self.y, 64, 55)
                #pygame.draw.rect(win, (255,0,0), self.hitbox, 2)

    def move(self):
        for i in range(self.num):
            self.x += self.vel
            if self.x <= 0:
                self.vel = 2
                self.x += self.vel
                self.y += 30
            elif self.x >= 730:
                self.vel = -2
                self.x += self.vel
                self.y += 30

class projectile(enemy):
    def __init__(self, enemy, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.vel = 40
        self.state = 'ready'

    def draw(self, win):
        bulletImg = pygame.image.load('bullet.png')
        win.blit(bulletImg, (self.x +16,self.y+ 10))
        self.state = 'fire'

    def hit(self, enemy):
        distance = math.sqrt(((enemy.hitbox[0] - self.x) ** 2) + ((enemy.hitbox[1] - self.y) ** 2))
        if distance < 27:
            return True
        else:
            return False

def game_over():
    font = pygame.font.SysFont('Algerian', 50, True, True)
    text = font.render('GAME OVER', 1, (255,255,255))
    win.blit(text, (250,300))

def redrawGameWindow():
    bg = pygame.image.load('background.png')
    win.blit(bg, (0,0))
    text = font.render('Score: '+ str(score), 1, (255,255,0))
    win.blit(text, (50,10))
    ship.draw(win)
    for i in range(6):
        aliens[i].draw(win)
    pygame.display.update()

run = True
ship = player(370, 480, 64, 64)
aliens = []
max_aliens = 6
for i in range(max_aliens):
    aliens.append(enemy(random.randint(0,736), random.randint(50,150), 64, 64))
    bullet = projectile(aliens[i], ship.x, 480, 32, 32)
font = pygame.font.SysFont('Algerian', 30, True, True)
while run:

    pygame.time.delay(1)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False

    keys = pygame.key.get_pressed()

    if keys[pygame.K_LEFT] and ship.x > ship.vel:
        ship.x -= ship.vel
    if keys[pygame.K_RIGHT] and ship.x < 800 - ship.width - ship.vel:
        ship.x += ship.vel
    if keys[pygame.K_SPACE]:
        if bullet.state is 'ready':
            bullet.x = ship.x
            bullet.draw(win)
            bulletSound.play()

    if bullet.y == 0:
        bullet.y = 480
        bullet.state = 'ready'

    if bullet.state is 'fire':
        bullet.y -= bullet.vel
        bullet.draw(win)
        pygame.display.update()

    for i in range(max_aliens):
        collision = bullet.hit(aliens[i])
        if collision:
            hitSound.play()
            bullet.y = 480
            bullet.state = 'ready'
            score += 10
            aliens[i].x = random.randint(0,735)
            aliens[i].y = random.randint(50,150)

    redrawGameWindow()

pygame.quit()
